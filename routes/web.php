<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
 */
Route::get('login', 'LoginController@Login')->middleware('checkLogout');
//route login request
Route::post('login', 'LoginController@postLogin');
//route logout
Route::get('admin/logout', 'LogoutController@Logout');
Route::get('home', 'HomeController@Home')->middleware('checkUrl');
//route group admin
Route::group(['prefix' => 'admin', 'middleware' => 'checkLogin'], function () {
    
    Route::resource('users', 'UserController')->middleware('checkUser');

    Route::resource('categories', 'CategoriesController');
    Route::post('categories/products', 'FilterProductsController@get');

    Route::resource('products', 'ProductsController');
    Route::post('products/search', 'SearchProductsController@search');

    Route::get('orders', 'OrderController@getAll');
});

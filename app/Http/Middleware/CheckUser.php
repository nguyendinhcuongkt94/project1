<?php

namespace App\Http\Middleware;

use Auth;
use Closure;

class CheckUser
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (Auth::user()->level == 0) {
            return $next($request);
        } else {
            return redirect()->back()->with('thongbao', 'Bạn không có quyền vào quản lý thành viên!');
        }
    }
}

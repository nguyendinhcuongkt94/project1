<?php

namespace App\Http\Controllers;

use App\models\Order;

class OrderController extends Controller
{
    public function getall()
    {
        $data['orders'] = Order::all();
        return view('backend.order.order', $data);
    }
}

<?php

namespace App\Http\Controllers;

use App\Http\Requests\Loginrequest;
use Auth;

class LoginController extends Controller
{
    public function Login()
    {
        return view('backend.login');
    }

    public function postLogin(Loginrequest $request)
    {
        $email = $request->email;
        $password = $request->password;
        if (Auth::attempt(['email' => $email, 'password' => $password])) {
            return redirect('admin/categories');
        } else {
            return redirect()->back()->with('thongbao', 'Email hoặc mật khẩu không đúng!');
        }
    }
}

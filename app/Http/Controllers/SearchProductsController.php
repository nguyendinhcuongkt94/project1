<?php

namespace App\Http\Controllers;

use App\models\Product;
use Illuminate\Http\Request;

class SearchProductsController extends Controller
{
    public function search(Request $request)
    {
        $products = Product::search($request);
        return view('backend.product.data_render', $products)->render();
    }
}

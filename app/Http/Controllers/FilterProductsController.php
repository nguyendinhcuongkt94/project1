<?php

namespace App\Http\Controllers;

use App\models\Category;
use Illuminate\Http\Request;

class FilterProductsController extends Controller
{
    public function get(request $request)
    {
        $data['products'] = Category::getProducts($request);
        return view('backend.category.render_list_product', $data)->render();
    }
}

<?php

namespace App\Http\Controllers;

use Auth;

class LogoutController extends Controller
{
    public function Logout()
    {
        Auth::logout();
        return redirect('login');
    }
}

<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    protected $table = "category";
    protected $fillable = ['id', 'name'];
    public $timestamps = false;

    public function products()
    {
        return $this->hasMany('App\models\Product', 'category_id', 'id');
    }

    public static function getAll()
    {
        $data['categories'] = Category::all();
        return $data;
    }

    public static function addNew($request)
    {
        Category::create($request->all());
    }
    
    public static function getProducts($request)
    {
        $category_id = $request->get('category_id');
        $products = Product::where('category_id', $category_id)->get();
        return $products;
    }
}

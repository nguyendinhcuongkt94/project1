<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;
class User extends Model
{
    protected $table = "users";
    protected $fillable = ['email', 'password', 'full', 'address', 'phone', 'level'];
    public $timestamps = false;
    
    public static function getAll()
    {
        $data['users'] = User::orderby('id', 'desc')->get();
        return $data;
    }

    public static function addNew($request)
    {
        $formData =[
            'email'=>$request->email,
            'password'=>bcrypt($request->password),
            'full'=>$request->full,
            'address'=>$request->address,
            'phone'=>$request->phone,
            'level'=>$request->level
        ];
        $user = User::create($formData);
        return $user;
    }

    public static function detail($id)
    {
        $user = User::find($id);
        return $user;
    }

    public static function edit($request, $id)
    {
        $formData =[
            'email'=>$request->email,
            'full'=>$request->full,
            'address'=>$request->address,
            'phone'=>$request->phone,
            'level'=>$request->level
        ];
        User::find($id)->update($formData);
        $user = User::find($id);
        return $user;
    }

    public static function remove($id)
    {
        User::find($id)->delete();
    }
    
}

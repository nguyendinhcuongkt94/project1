<?php

namespace App\models;

use App\models\Category;
use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $table = 'product';
    protected $fillable = ['code', 'name', 'price', 'describe', 'img', 'category_id'];

    public function category()
    {
        return $this->belongsTo('App\models\Category', 'category_id', 'id');
    }

    public static function getAll()
    {
        $data['products'] = Product::orderby('id', 'desc')->get();
        $data['categorys'] = Category::all();
        return $data;
    }

    public static function addNew($request)
    {
        $img = $request->file('img');
        $imgNewName = rand() . '.' . $img->getClientOriginalExtension();
        $img->move('admins/img', $imgNewName);
        $formData = [
            'name' => $request->name,
            'code' => $request->code,
            'price' => $request->price,
            'describe' => $request->describe,
            'category_id' => $request->category_id,
            'img' => $imgNewName,
        ];
        $data = Product::create($formData);
        return $data;
    }

    public static function detail($id)
    {
        $getproduct = Product::find($id);
        return $getproduct;
    }

    public static function edit($request, $id)
    {
        $image = $request->hidden_image;
        $imageNew = $request->file('img');
        if ($imageNew != '') {
            $image = rand() . '.' . $imageNew->getClientOriginalExtension();
            $image->move('admins/img', $image);
        }
        $formData = [
            'name' => $request->name,
            'code' => $request->code,
            'price' => $request->price,
            'describe' => $request->describe,
            'category_id' => $request->category_id,
            'img' => $image,
        ];
        Product::find($id)->update($formData);
        $data = Product::find($id);
        return $data;
    }

    public static function remove($id)
    {
        Product::find($id)->delete();
    }

    public static function search($request)
    {
        $key = $request->get('key');
        $data['products'] = Product::orwhere('name', 'like', '%' . $key . '%')->orwhere('code', 'like', '%' . $key . '%')
            ->orwhere('describe', 'like', '%' . $key . '%')->orwhere('code', 'like', '%' . $key . '%')
            ->orderby('id', 'desc')->get();
        return $data;
    }
}

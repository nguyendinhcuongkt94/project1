<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    protected $table = "orders";
    public $timestamps = false;
    public function products()
    {
        return $this->belongsToMany('App\models\Product', 'order_product', 'order_id', 'product_id');
    }
}

$(document).ready(function() {
    $.ajaxSetup({
        headers: {
            "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content")
        }
    });
    //add a new user
    $("#create-user").click(function() {
        $("#modal-add").modal("show");
    });
    $("#form-add").submit(function(e) {
        e.preventDefault();
        $.ajax({
            type: "POST",
            url: "/admin/users",
            contentType: false,
            cache: false,
            processData: false,
            data: new FormData(this),
            success: function(response) {
                $("#modal-add").modal("hide");
                $("#tbody").prepend(dataTable(response.data));
            },
            error: function(error) {
                $(".alert-danger").remove();
                for (var i in error.responseJSON.errors) {
                    $("#alert_error").append(
                        `<p class='alert-danger'>` +
                            error.responseJSON.errors[i] +
                        `</p>`
                    );
                }
            }
        });
    });
    //show user
    $(document).on("click", ".edit-user", function() {
        var id = $(this).attr("data-id");
        $("#modal-edit").modal("show");
        $.ajax({
            type: "get",
            url: "/admin/users/" + id,
            success: function(response) {
                $("#email-edit").val(response.data.email);
                $("#fullname-edit").val(response.data.full);
                $("#address-edit").val(response.data.address);
                $("#telephone-edit").val(response.data.phone);
                $("#level-edit").val(response.data.level);
                $("#hiden_id").val(response.data.id);
            }
        });
    });
    //edit user
    $("#form-edit").submit(function(e) {
        e.preventDefault();
        var id = $("#hiden_id").val();
        $.ajax({
            type: "put",
            url: "/admin/users/" + id,
            data: $("#form-edit").serialize(),
            success: function(response) {
                $("#modal-edit").modal("hide");
                $("#tr--" + id).replaceWith(dataTable(response.data));
            },
            error: function(error) {
                $(".alert-danger").remove();
                for (var i in error.responseJSON.errors) {
                    $("#alert_error_edit").append(
                        `<p class='alert-danger'>` +
                            error.responseJSON.errors[i] +
                        `</p>`
                    );
                }
            }
        });
    });
    //delete-user
    $(document).on("click", ".delete-user", function() {
        $("#modal-delete").modal("show");
        var id = $(this).attr("data-id");
        $.ajax({
            type: "get",
            url: "/admin/users/" + id,
            success: function(response) {
                $("#userdelete").text(response.data.full);
                $("#form-delete").attr("data-id", response.data.id);
            }
        });
    });
    $("#form-delete").submit(function(e) {
        e.preventDefault();
        $("#modal-delete").modal("hide");
        var id = $(this).attr("data-id");
        $.ajax({
            type: "delete",
            url: "/admin/users/" + id,
            success: function(response) {
                $("#tr--" + response.userid).remove();
            }
        });
    });
});

function dataTable(data) {
    var html = "";
    html +=
        `<tr id="tr--` + data.id + `">
            <td>` + data.id + `</td>
            <td>` + data.email + `</td>
            <td>` + data.full + `</td>
            <td>` + data.address + `</td>
            <td>` + data.phone + `</td>
            <td>` + data.level + `</td>
            <td>
            <button data-id="` + data.id + `" class='btn btn-warning edit-user'><i class='fa fa-pencil'
                    aria-hidden='true'></i> Sửa</button>
            <button data-id="` + data.id + `" class='btn btn-danger delete-user'><i class='fa fa-trash'
                    aria-hidden='true'></i> Xóa</button>
            </td>
        </tr>`;
    return html;
}
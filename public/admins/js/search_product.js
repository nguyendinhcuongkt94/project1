$(document).ready(function () {
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    $('#btn-add-product').click(function () {
        $('#action_button').val("Add");
        $('#action').val("Add");
        $('#modal-add-product').modal('show');
        $('.modal-title').text('Add product');
        $('#name').val('');
        $('#code').val('');
        $('#price').val('');
        $('#describe').val('');
        $('#img').val('');
        $('.img-thumbnail').remove();
        $('.alert-danger').remove();
    });
    //add product
    $("#form-add-product").submit(function (e) {
        e.preventDefault();
        var url = $(this).attr('data-url');
        if ($('#action').val() == 'Add') {
            $.ajax({
                type: "POST",
                url: url,
                contentType: false,
                cache: false,
                processData: false,
                dataType: "json",
                data: new FormData(this),
                success: function (data) {
                    $('#modal-add-product').modal('hide');
                    location.reload(true);
                    html = '<div class="alert alert-success">' + data + '</div>';
                    $('#form_result').html(html);
                },
                error: function (error) {
                    $('.alert-danger').remove();
                    for (var i in error.responseJSON.errors) {
                        $('#alert_error').append(`
                            <p class='alert-danger'>` + error.responseJSON.errors[i] + `</p>
                        `)
                    }
                }
            });
        }
        //edit product
        if ($('#action').val() == "Edit") {
            var url = $('#modal-add-product').attr('data-url');
            $.ajax({
                type: "PATCH",
                url: url,
                cache: false,
                processData: false,
                data: $('#form-add-product').serialize(),
                success: function (data) {
                    console.log(data.success);
                    $('modal-edit-product').modal('hide');
                    $('#action').val("Add");
                    location.reload();
                },
                error: function (error) {
                    $('.alert-danger').remove();
                    for (var i in error.responseJSON.errors) {
                        $('#alert_error').append(`
                        <p class='alert-danger'>` + error.responseJSON.errors[i] + `</p>
                    `)
                    }
                }
            });
        }
    });
    //show product
    $('.btn_edit_product').click(function (e) {
        $('.alert-danger').remove();
        $('#modal-add-product').modal('show');
        $('.modal-title').text('Edit product');
        var url = $(this).attr('data-url');
        $.ajax({
            type: "get",
            url: url,
            success: function (response) {
                $("#code").val(response.data.code);
                $("#name").val(response.data.name);
                $("#price").val(response.data.price);
                $("#describe").val(response.data.describe);
                $("#category_id").val(response.data.category_id);
                $('#store_image').html("<img src='/admins/img/" + response.data.img + "' width='70' class='img-thumbnail' />");
                $('#store_image').append("<input type='hidden' name='hidden_image' value='" + response.data.img + "' />");
                $('#hidden_id').val(response.data.id);
                $('#action').val("Edit");
                $('#action_button').val('Edit');
            }
        });
    });
    //delete product
    $('.btn_delete_product').click(function (e) {
        e.preventDefault();
        $('#modal-delete-product').modal('show');
        var url = $(this).attr('data-url')
        $.ajax({
            type: "get",
            url: url,
            success: function (response) {
                $('#productdelete').text(response.data.name);
                $('#form-delete-product').attr('data-url', response.data.id)
            }
        });
    });
    $('#form-delete-product').submit(function (e) {
        e.preventDefault();
        $('#modal-delete-product').modal('hide');
        var id = $(this).attr('data-url');
        $.ajax({
            type: "delete",
            url: '/admin/products/' + id,
            success: function (response) {
                $('#tr--' + response.productid).remove();
                $('#form_result').text(response.message);
            }
        });
    });
});

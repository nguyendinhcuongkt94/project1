$(document).ready(function () {
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    //show all products in category
    $('#category_change').change(function () {
        var category_id = $(this).val();
        $.ajax({
            type: "POST",
            url: "/admin/categories/products",
            data: {
                category_id: category_id
            },
            success: function (data) {
                $('#data_render').html(data);
            }
        });
    });
    //add category
    $('#addcategory').click(function (e) {
        e.preventDefault();
        $('.alert-danger').remove();
        $('#modal-add-category').modal('show');
    });
    $("#form-add-category").submit(function (e) {
        e.preventDefault();
        url = $(this).attr('data-url');
        $.ajax({
            type: "post",
            url: url,
            contentType: false,
            cache: false,
            processData: false,
            data: {
                name: $('#category_name').val(),
            },
            success: function (response) {
                $('#modal-add-category').modal('hide');
                $('#form-result').append(`
                <p class='alert-danger'>` + response.data + `</p>`);
            },
            error: function (error) {
                $('.alert-danger').remove();
                for (var i in error.responseJSON.errors) {
                    $('#aleart_errors_category').append(`
                    <p class='alert-danger'>` + error.responseJSON.errors[i] + `</p>
                    `)
                }
            }
        });
    })

});

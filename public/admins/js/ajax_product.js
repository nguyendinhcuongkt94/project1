$(document).ready(function () {
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    $('#btn-add-product').click(function () {
        $('#action_button').val("Add");
        $("#form-add-product").trigger('reset');
        $('#store_image').html('');
        $('#action').val("Add");
        $('#modal-add-product').modal('show');
        $('.modal-title').text('Add product');
        $('.alert-danger').remove();
    });
    //add product
    $("#form-add-product").submit(function (e) {
        e.preventDefault();
        var url = $(this).attr('data-url');
        if ($('#action').val() == 'Add') {
            $.ajax({
                type: "POST",
                url: url,
                contentType: false,
                cache: false,
                processData: false,
                dataType: "json",
                data: new FormData(this),
                success: function (response) {
                    $('#modal-add-product').modal('hide');
                    $('.tbody_product').prepend(dataTable(response.data));
                    alert(response.success);
                },
                error: function (error) {
                    $('.alert-danger').remove();
                    for (var i in error.responseJSON.errors) {
                        $('#alert_error').append(`
                            <p class='alert-danger'>` + error.responseJSON.errors[i] + `</p>
                        `)
                    }
                }
            });
        }
        //edit product
        if ($('#action').val() == "Edit") {
            var url = $('#modal-add-product').attr('data-url');
            var id = $('#hidden_id').val();
            $.ajax({
                type: "PUT",
                url: "/admin/products/" + id,
                cache: false,
                processData: false,
                data: $('#form-add-product').serialize(),
                success: function (response) {
                    $('#modal-add-product').modal('hide');
                    $('#action').val("Add");
                    $('#tr--' + id).replaceWith(dataTable(response.data));
                    alert(response.success);
                },
                error: function (error) {
                    $('.alert-danger').remove();
                    for (var i in error.responseJSON.errors) {
                        $('#alert_error').append(`
                        <p class='alert-danger'>` + error.responseJSON.errors[i] + `</p>
                    `)
                    }
                }
            });
        }
    });
    //show information product
    $(document).on("click", ".btn_edit_product", function () {
        $('.alert-danger').remove();
        $('#modal-add-product').modal('show');
        $('.modal-title').text('Edit product');
        var id = $(this).attr('data-id');
        $.ajax({
            type: "get",
            url: "/admin/products/" + id,
            success: function (response) {
                $("#code").val(response.data.code);
                $("#name").val(response.data.name);
                $("#price").val(response.data.price);
                $("#describe").val(response.data.describe);
                $("#category_id").val(response.data.category_id);
                $('#store_image').html("<img src='/admins/img/" + response.data.img + "' width='70' class='img-thumbnail' />");
                $('#store_image').append("<input type='hidden' name='hidden_image' value='" + response.data.img + "' />");
                $('#hidden_id').val(response.data.id);
                $('#action').val("Edit");
                $('#action_button').val('Edit');
            }
        });
    });
    //delete product
    $(document).on("click", ".btn_delete_product", function (e) {
        e.preventDefault();
        $('#modal-delete-product').modal('show');
        var id = $(this).attr('data-id');
        $.ajax({
            type: "get",
            url: "/admin/products/" + id,
            success: function (response) {
                $('#productdelete').text(response.data.name);
                $('#form-delete-product').attr('data-url', response.data.id)
            }
        });
    });
    $('#form-delete-product').submit(function (e) {
        e.preventDefault();
        $('#modal-delete-product').modal('hide');
        var id = $(this).attr('data-url');
        $.ajax({
            type: "delete",
            url: '/admin/products/' + id,
            success: function (response) {
                $('#tr--' + response.productid).remove();
                $('#form_result').text(response.message);
            }
        });
    });
    //search product
    $('#search-product').keyup(function () {
        var key = $('#search-product').val();
        $.ajax({
            type: "post",
            url: "/admin/products/search",
            data: {
                'key': key
            },
            success: function (data) {
                $('.tbody_product').html(data);
            }
        });
    });
});

function dataTable(data) {
    var html = "";
    html += `<tr id="tr--` + data.id + `">
        <td>` + data.id + `</td>
        <td>
            <div class="row">
                <div class="col-md-3"><img src="/admins/img/` + data.img + `"
                        alt="Áo đẹp" width="100px" class="thumbnail"></div>
                <div class="col-md-9">
                    <p><strong>Mã sản phẩm :` + data.code + ` </strong></p>
                    <p>Tên sản phẩm :` + data.name + `</p>
                    <p>Danh mục:</p>
                </div>
            </div>
        </td>
        <td>` + data.price + `VND</td>
        <td>
            <p>` + data.describe + `</p>
        </td>
        <td></td>
        <td>
            <button  data-id="` + data.id + `"
            class="btn btn-warning btn_edit_product "><i class="fa fa-pencil"
                aria-hidden="true"></i> Sửa</button>
            <button  data-id="` + data.id + `" 
                class="btn btn-danger btn_delete_product"><i class="fa fa-trash"
                    aria-hidden="true"></i> Xóa</button>
        </td>
    </tr>`;
    return html;
}
